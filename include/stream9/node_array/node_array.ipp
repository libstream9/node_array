#ifndef STREAM9_NODE_ARRAY_IPP
#define STREAM9_NODE_ARRAY_IPP

#include <algorithm>

#include <stream9/errors.hpp>
#include <stream9/iterators.hpp>
#include <stream9/json.hpp>

namespace stream9 {

template<typename T>
class node_array_iterator
    : public iterators::iterator_facade<node_array_iterator<T>,
                                std::random_access_iterator_tag,
                                T& >
{
    using base_t = typename array<node<T>>::iterator;

public:
    node_array_iterator() = default;

    node_array_iterator(base_t const base) noexcept
        : m_base { base }
    {}

    node_array_iterator(node_array_iterator const&) = default;
    node_array_iterator& operator=(node_array_iterator const&) = default;

    node_array_iterator(node_array_iterator&&) = default;
    node_array_iterator& operator=(node_array_iterator&&) = default;

    base_t base() const noexcept
    {
        return m_base;
    }

    operator base_t () const noexcept
    {
        return m_base;
    }

private:
    friend class iterators::iterator_core_access;

    T&
    dereference() const noexcept
    {
        return **m_base;
    }

    void increment() noexcept
    {
        ++m_base;
    }

    void decrement() noexcept
    {
        --m_base;
    }

    std::ptrdiff_t
    distance_to(node_array_iterator const other) const noexcept
    {
        return other.m_base - m_base;
    }

    void
    advance(std::ptrdiff_t const n) noexcept
    {
        m_base += n;
    }

    bool equal(node_array_iterator const other) const noexcept
    {
        return m_base == other.m_base;
    }

    std::strong_ordering
    compare(node_array_iterator const other) const noexcept
    {
        return m_base <=> other.m_base;
    }

private:
    base_t m_base {};
};

template<typename T>
class const_node_array_iterator
    : public iterators::iterator_facade<const_node_array_iterator<T>,
                                std::random_access_iterator_tag,
                                T const& >
{
    using base_t = typename array<node<T>>::const_iterator;

public:
    const_node_array_iterator() = default;

    const_node_array_iterator(base_t base) noexcept
        : m_base { base }
    {}

    const_node_array_iterator(node_array_iterator<T> o) noexcept
        : m_base { o.base() }
    {}

    const_node_array_iterator(const_node_array_iterator const&) = default;
    const_node_array_iterator& operator=(const_node_array_iterator const&) = default;

    const_node_array_iterator(const_node_array_iterator&&) = default;
    const_node_array_iterator& operator=(const_node_array_iterator&&) = default;

    base_t base() const noexcept
    {
        return m_base;
    }

    operator base_t () const noexcept
    {
        return m_base;
    }

private:
    friend class iterators::iterator_core_access;

    T const&
    dereference() const noexcept
    {
        return **m_base;
    }

    void increment() noexcept
    {
        ++m_base;
    }

    void decrement() noexcept
    {
        --m_base;
    }

    std::ptrdiff_t
    distance_to(const_node_array_iterator other) const noexcept
    {
        return other.m_base - m_base;
    }

    void
    advance(std::ptrdiff_t n) noexcept
    {
        m_base += n;
    }

    bool equal(const_node_array_iterator other) const noexcept
    {
        return m_base == other.m_base;
    }

    std::strong_ordering
    compare(const_node_array_iterator other) const noexcept
    {
        return m_base <=> other.m_base;
    }

private:
    base_t m_base {};
};

template<typename T, typename A>
node_array<T, A>::
node_array() noexcept = default;

template<typename T, typename A>
template<std::input_iterator I>
node_array<T, A>::
node_array(I first, I last)
    requires std::is_constructible_v<T, std::iter_reference_t<I>>
{
    if constexpr (std::sized_sentinel_for<I, I>) {
        safe_integer const len = std::distance(first, last);
        m_arr.reserve(len);
    }

    while (first != last) {
        m_arr.emplace(*first);
        ++first;
    }
}

template<typename T, typename A>
node_array<T, A>::
node_array(std::initializer_list<T> ilist)
    requires std::is_copy_constructible_v<T>
{
    m_arr.reserve(ilist.size());

    for (auto& v: ilist) {
        m_arr.emplace(std::move(v));
    }
}

template<typename T, typename A>
node_array<T, A>::iterator node_array<T, A>::
begin() noexcept
{
    return m_arr.begin();
}

template<typename T, typename A>
node_array<T, A>::iterator node_array<T, A>::
end() noexcept
{
    return m_arr.end();
}

template<typename T, typename A>
node_array<T, A>::const_iterator node_array<T, A>::
begin() const noexcept
{
    return m_arr.begin();
}

template<typename T, typename A>
node_array<T, A>::const_iterator node_array<T, A>::
end() const noexcept
{
    return m_arr.end();
}

template<typename T, typename A>
node_array<T, A>::reference node_array<T, A>::
front() noexcept
{
    return *m_arr.front();
}

template<typename T, typename A>
node_array<T, A>::const_reference node_array<T, A>::
front() const noexcept
{
    return *m_arr.front();
}

template<typename T, typename A>
node_array<T, A>::reference node_array<T, A>::
back() noexcept
{
    return *m_arr.back();
}

template<typename T, typename A>
node_array<T, A>::const_reference node_array<T, A>::
back() const noexcept
{
    return *m_arr.back();
}

template<typename T, typename A>
node_array<T, A>::reference node_array<T, A>::
at(size_type pos)
{
    if (pos >= size()) {
        throw error {
            make_error_code(std::errc::argument_out_of_domain), {
                { "pos", pos },
                { "size", size() }
            }
        };
    }

    return *m_arr.at(pos);
}

template<typename T, typename A>
node_array<T, A>::const_reference node_array<T, A>::
at(size_type pos) const
{
    if (pos >= size()) {
        throw error {
            make_error_code(std::errc::argument_out_of_domain), {
                { "pos", pos },
                { "size", size() }
            }
        };
    }

    return *m_arr.at(pos);
}

template<typename T, typename A>
node_array<T, A>::reference node_array<T, A>::
operator[](size_type pos)
{
    if (pos >= size()) {
        throw error {
            make_error_code(std::errc::argument_out_of_domain), {
                { "pos", pos },
                { "size", size() }
            }
        };
    }

    return *m_arr[pos];
}

template<typename T, typename A>
node_array<T, A>::const_reference node_array<T, A>::
operator[](size_type pos) const
{
    if (pos >= size()) {
        throw error {
            make_error_code(std::errc::argument_out_of_domain), {
                { "pos", pos },
                { "size", size() }
            }
        };
    }

    return *m_arr[pos];
}

template<typename T, typename A>
node_array<T, A>::size_type node_array<T, A>::
size() const noexcept
{
    return m_arr.size();
}

template<typename T, typename A>
node_array<T, A>::size_type node_array<T, A>::
max_size() const noexcept
{
    return m_arr.max_size();
}

template<typename T, typename A>
node_array<T, A>::size_type node_array<T, A>::
capacity() const noexcept
{
    return m_arr.capacity();
}

template<typename T, typename A>
bool node_array<T, A>::
empty() const noexcept
{
    return m_arr.empty();
}

// natural insert
template<typename T, typename A>
node_array<T, A>::iterator node_array<T, A>::
insert(T const& v)
    requires std::is_copy_constructible_v<T>
{
    try {
        return m_arr.emplace(v);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename A>
node_array<T, A>::iterator node_array<T, A>::
insert(T&& v)
    requires std::is_move_constructible_v<T>
{
    try {
        return m_arr.emplace(std::move(v));
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename A>
template<typename U/*= T*/>
node_array<T, A>::iterator node_array<T, A>::
insert(node<U> v)
    requires (std::same_as<U, T> ||
              (std::is_polymorphic_v<U> && std::is_base_of_v<T, U>) )
{
    try {
        return m_arr.insert(static_cast<node<T>&&>(std::move(v)));
    }
    catch (...) {
        rethrow_error();
    }
}

// positional insert
template<typename T, typename A>
node_array<T, A>::iterator node_array<T, A>::
insert(const_iterator pos, T const& v)
    requires std::is_copy_constructible_v<T>
{
    try {
        return m_arr.emplace(pos.base(), v);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename A>
node_array<T, A>::iterator node_array<T, A>::
insert(const_iterator pos, T&& v)
    requires std::is_move_constructible_v<T>
{
    try {
        return m_arr.emplace(pos.base(), std::move(v));
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename A>
node_array<T, A>::iterator node_array<T, A>::
insert(const_iterator pos, node<T> ptr)
{
    try {
        return m_arr.insert(pos.base(), std::move(ptr));
    }
    catch (...) {
        rethrow_error();
    }
}

// positional range insert
template<typename T, typename A>
template<std::input_iterator I, std::sentinel_for<I> S>
node_array<T, A>::iterator node_array<T, A>::
insert(const_iterator pos, I first, S last)
    requires std::is_constructible_v<T, std::iter_reference_t<I>>
{
    try {
        if (first == last) {
            return m_arr.remove_const(pos);
        }

        auto idx = pos.base() - m_arr.begin();

        if constexpr (std::sized_sentinel_for<S, I>) {
            safe_integer len = std::distance(first, last);
            m_arr.reserve(m_arr.size() + len);
        }

        auto first_it = m_arr.emplace(m_arr.begin() + idx, *first);
        auto it = first_it;

        while (++first != last) {
            it = m_arr.emplace(++it, *first);
        }

        return first_it;
    }
    catch (...) {
        rethrow_error();
    }
}

// natural emplace
template<typename T, typename A>
template<typename U/*= T*/, typename... Args>
std::pair<typename node_array<T, A>::iterator, U&> node_array<T, A>::
emplace(Args&&... args)
    requires std::is_constructible_v<U, Args...>
          && (std::same_as<U, T> ||
              (std::is_polymorphic_v<U> && std::is_base_of_v<T, U>) )
{
    try {
        auto it = m_arr.insert(node<U>(std::forward<Args>(args)...));
        auto& r = static_cast<U&>(**it);
        return { it, r };
    }
    catch (...) {
        rethrow_error();
    }
}

// positional emplace
template<typename T, typename A>
template<typename U/*= T*/, typename... Args>
std::pair<typename node_array<T, A>::iterator, U&> node_array<T, A>::
emplace(const_iterator pos, Args&&... args)
    requires std::is_constructible_v<U, Args...>
          && (std::same_as<U, T> ||
              (std::is_polymorphic_v<U> && std::is_base_of_v<T, U>) )
{
    try {
        auto it = m_arr.emplace(
            pos.base(), std::forward<Args>(args)...);
        return { it, static_cast<U&>(**it) };
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename A>
node_array<T, A>::iterator node_array<T, A>::
erase(const_iterator pos) noexcept
{
    return m_arr.erase(pos);
}

template<typename T, typename A>
node_array<T, A>::iterator node_array<T, A>::
erase(const_iterator first, const_iterator last) noexcept
{
    return m_arr.erase(first, last);
}

template<typename T, typename A>
template<typename Pred, typename Proj/*= std::identity*/>
size_t node_array<T, A>::
erase_if(Pred pred, Proj proj/*= {}*/) noexcept
    requires requires (Proj proj, reference r) { proj(r); }
          && requires (Pred pred, reference r) { { pred(proj(r)) } -> std::convertible_to<bool>; }
{
    namespace rng = std::ranges;

    auto removed = rng::remove_if(m_arr,
        std::move(pred),
        [&](auto&& n) -> decltype(auto) { return proj(*n); }
    );
    m_arr.erase(removed.begin(), removed.end());

    return rng::size(removed);
}

template<typename T, typename A>
void node_array<T, A>::
clear() noexcept
{
    m_arr.clear();
}

template<typename T, typename A>
void node_array<T, A>::
resize(size_type count)
    requires std::is_default_constructible_v<T>
{
    try {
        if (size() >= count) {
            m_arr.resize(count);
        }
        else {
            m_arr.reserve(count);

            for (auto i = size(); i < count; ++i) {
                m_arr.emplace();
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename A>
void node_array<T, A>::
resize(size_type const count, value_type const& value)
    requires std::is_copy_constructible_v<T>
{
    try {
        if (size() >= count) {
            m_arr.resize(count);
        }
        else {
            m_arr.reserve(count);

            for (auto i = size(); i < count; ++i) {
                m_arr.emplace(value);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename A>
void node_array<T, A>::
reserve(size_type new_cap)
{
    try {
        m_arr.reserve(new_cap);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename A>
void node_array<T, A>::
shrink_to_fit()
{
    try {
        m_arr.shrink_to_fit();
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename A>
void node_array<T, A>::
swap(node_array& other) noexcept
{
    m_arr.swap(other.m_arr);
}

template<typename T>
void
iter_swap(node_array_iterator<T> pos, node<T>& n) noexcept
{
    using std::swap;
    swap(*pos.base(), n);
}

template<typename T, typename A>
bool node_array<T, A>::
operator==(node_array const& other) const
{
    namespace rng = std::ranges;
    return rng::equal(*this, other);
}

template<typename T, typename A>
auto node_array<T, A>::
operator<=>(node_array const& other) const
{
    return std::lexicographical_compare_three_way(
        begin(), end(),
        other.begin(), other.end()
    );
}

template<typename T, typename Alloc>
void
swap(node_array<T, Alloc>& lhs, node_array<T, Alloc>& rhs) noexcept
{
    lhs.swap(rhs);
}

namespace json {

template<typename T> struct type_of;

template<typename T, typename A>
void tag_invoke(value_from_tag, value& v, stream9::node_array<T, A> const& a)
{
    try {
        auto& arr = v.emplace_array();

        for (auto const& e: a) {
            arr.push_back(e);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace json

} // namespace stream9

#endif // STREAM9_NODE_ARRAY_IPP
