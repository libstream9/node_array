#ifndef STREAM9_NODE_ARRAY_NODE_ARRAY_HPP
#define STREAM9_NODE_ARRAY_NODE_ARRAY_HPP

#include <algorithm>
#include <cstdint>
#include <initializer_list>
#include <iterator>
#include <memory>
#include <type_traits>
#include <vector>

#include <stream9/array.hpp>
#include <stream9/node.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9 {

template<typename> class node_array_iterator;
template<typename> class const_node_array_iterator;

template<typename T,
         typename Allocator = std::allocator<T> >
class node_array
{
    static_assert(!std::is_reference_v<T>);
    static_assert(!std::is_const_v<T>);

    using array_t = array<node<T>>;
public:
    using iterator = node_array_iterator<T>;
    using const_iterator = const_node_array_iterator<T>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    using size_type = safe_integer<std::int64_t, 0>;
    using value_type = T;
    using reference = value_type&;
    using const_reference = value_type const&;
    using pointer = std::add_pointer_t<value_type>;
    using const_pointer = std::add_pointer_t<value_type const>;
    using allocator_type = Allocator;
    using difference_type = safe_integer<std::int64_t, 0>;

public:
    // essential
    node_array() noexcept;

    template<std::input_iterator I>
    node_array(I first, I last)
        requires std::is_constructible_v<T, std::iter_reference_t<I>>;

    //TODO range constructor
    node_array(std::initializer_list<T> ilist)
        requires std::is_copy_constructible_v<T>;

    node_array(node_array const&) = delete;
    node_array& operator=(node_array const&) = delete;

    node_array(node_array&&) = default;
    node_array& operator=(node_array&&) = default;

    ~node_array() = default;

    // accessor
    iterator begin() noexcept;
    iterator end() noexcept;

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    reference front() noexcept;
    const_reference front() const noexcept;

    reference back() noexcept;
    const_reference back() const noexcept;

    reference at(size_type pos);
    const_reference at(size_type pos) const;

    reference operator[](size_type pos);
    const_reference operator[](size_type pos) const;

    // query
    size_type size() const noexcept;

    size_type max_size() const noexcept;

    size_type capacity() const noexcept;

    bool empty() const noexcept;

    // modifier
    iterator insert(T const&)
        requires std::is_copy_constructible_v<T>;

    iterator insert(T&&)
        requires std::is_move_constructible_v<T>;

    template<typename U = T>
    iterator insert(node<U>)
        requires (std::same_as<U, T> ||
                  (std::is_polymorphic_v<U> && std::is_base_of_v<T, U>) );

    //TODO natural range insert

    iterator insert(const_iterator pos, T const&) //TODO maybe insert_at
        requires std::is_copy_constructible_v<T>;

    iterator insert(const_iterator pos, T&&)
        requires std::is_move_constructible_v<T>;

    iterator insert(const_iterator pos, node<T>);

    template<std::input_iterator I, std::sentinel_for<I> S>
    iterator insert(const_iterator pos, I first, S last)
        requires std::is_constructible_v<T, std::iter_reference_t<I>>;

    //TODO range insert

    template<typename U = T, typename... Args>
    std::pair<iterator, U&>
    emplace(Args&&... args)
        requires std::is_constructible_v<U, Args...>
              && (std::same_as<U, T> ||
                  (std::is_polymorphic_v<U> && std::is_base_of_v<T, U>) );

    template<typename U = T, typename... Args>
    std::pair<iterator, U&>
    emplace(const_iterator pos, Args&&... args)
        requires std::is_constructible_v<U, Args...>
              && (std::same_as<U, T> ||
                  (std::is_polymorphic_v<U> && std::is_base_of_v<T, U>) );

    iterator erase(const_iterator pos) noexcept;
    iterator erase(const_iterator first, const_iterator last) noexcept;

    //TODO Non-adhock solution.
    //     Define indirect_move() and implement remove_if in term of it,
    //     then generic erase_if works well on this type of container too.
    template<typename Pred, typename Proj = std::identity>
    size_t erase_if(Pred pred, Proj proj = {}) noexcept
        requires requires (Proj proj, reference r) { proj(r); }
              && requires (Pred pred, reference r) { { pred(proj(r)) } -> std::convertible_to<bool>; };

    //node<T> extract(const_iterator pos); //TODO

    void clear() noexcept;

    void resize(size_type count) //TODO variadic arguments
        requires std::is_default_constructible_v<T>;

    void resize(size_type count, value_type const& value)
        requires std::is_copy_constructible_v<T>;

    void reserve(size_type new_cap);

    void shrink_to_fit();

    void swap(node_array& other) noexcept;

    // comparison
    bool operator==(node_array const& other) const; //TODO noexcept
    auto operator<=>(node_array const& other) const;

    template<typename T1, typename Alloc>
    friend void
    swap(node_array<T1, Alloc>& lhs, node_array<T1, Alloc>& rhs) noexcept;

private:
    array_t m_arr;
};

template<typename T>
void
iter_swap(node_array_iterator<T> pos, node<T>& n) noexcept;

template<std::input_iterator It,
       typename Alloc = std::pmr::polymorphic_allocator<std::iter_value_t<It>> >
node_array(It, It, Alloc = Alloc())
    -> node_array<std::iter_value_t<It>, Alloc>;

} // namespace stream9

#include "node_array.ipp"

#endif // STREAM9_NODE_ARRAY_NODE_ARRAY_HPP
